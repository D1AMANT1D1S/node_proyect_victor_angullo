const express = require('express');
const controller = require('../controllers/artists.controller');
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');
const { isAdmin } = require('../middlewares/auth.middleware');
const router = express.Router();

router.get('/', controller.findArtist );

router.get('/create-artist', controller.createArtistGet);

router.post('/create', controller.newArtist);

router.get('/:id', controller.findArtistById);

router.put('/edit', controller.artistEditPut);


router.put('/artist/:id/add-to-study', controller.addToStudy);

router.put('/add-study', controller.addStudy);



  module.exports = router;