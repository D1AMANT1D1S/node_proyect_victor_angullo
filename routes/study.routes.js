const express = require('express');
const {
    allStudyGet,
    createStudyPost,
    addArtistPut,
    studyByIdGet,
    deleteStudyByIdPost,
} = require('../controllers/studies.controller');

const router = express.Router();


router.get('/', allStudyGet);

router.post('/create', createStudyPost);

router.put('/add-pet', addArtistPut);

router.get('/:id', studyByIdGet);
router.post('/:id', deleteStudyByIdPost);

module.exports = router;