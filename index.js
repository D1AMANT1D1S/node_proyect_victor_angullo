const express = require("express");
const path = require("path");
const hbs = require('hbs');
const session = require("express-session");
const MongoStore = require("connect-mongo");
const passport = require("passport");
const dotenv = require('dotenv');
dotenv.config();

const { isAuthenticated } = require('./middlewares/auth.middleware');

const indexRoutes = require("./routes/index.routes");
const artistsRoutes = require("./routes/artists.routes");
const authRoutes = require("./routes/auth.routes");
const studyRoutes = require("./routes/study.routes");

const db = require("./db.js");
require("./authentication");


const PORT = 3000;
const server = express();


db.connect();

server.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 15 * 24 * 60 * 60 * 1000,
        },
        store: MongoStore.create({ mongoUrl: db.DB_URL }),
    })
);

server.use(passport.initialize());
server.use(passport.session());


server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'hbs');

hbs.registerHelper('mayorque', (a, b, options) => {
    if (a > b) {
        return options.fn(this);
    } else {
        options.inverse(this);
    }
});

hbs.registerHelper('mayus', (string) => {
    return string.toUpperCase();
});

server.use(express.static(path.join(__dirname, 'public')));

server.use(express.json());

server.use(express.urlencoded({ extended: true }));



server.use("/", indexRoutes);
server.use("/artists", isAuthenticated, artistsRoutes);
server.use("/study", isAuthenticated, studyRoutes);
server.use("/auth", authRoutes);

server.use("*", (req, res) => {
    const error = new Error("Ruta no encontrada");
    error.status = 404;
    return res.status(404).json(error.message);
});


server.use((error, req, res, next) => {
    console.log("Error Handler", error);
    const errorStatus = error.status || 500;
    const errorMsg = error.message || "Unexpected Error";

    return res.status(errorStatus).json(errorMsg);
});

server.listen(PORT, () => {
    console.log(
        `Servidor comiendo en http://localhost:${PORT}`
    );
});