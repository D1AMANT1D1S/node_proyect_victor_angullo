const Artist = require('../models/artist.models');




const findArtist = async (req, res, next) => {
    try {
        const artists = await Artist.find();
        
        return res.render('artists', {artists: artists})
    } catch(error) {
        console.log('error', error);
        return next(error);
    }
};

const artistEditPut = async (req, res, next) => {
    try {
        const { id, name, style, description, freelance, location } = req.body;

        const editedArtist = await Artist.findByIdAndUpdate(
            id,
            { name },
            { style },
            { description },
            { freelance },
            { location },
            { new: true }
        );

        return res.status(200).json(editedArtist);
        
    } catch (error) {
        return next(error);
    }
};



const createArtistGet =  (req, res, next) => {
    return res.render('create-artist');
};

const newArtist = async (req, res, next) => {
    try {

        const { name, age, style, description, study, freelance, location } = req.body;

        const newArtist = new Artist ({ name, age, style, description, study, freelance, location });
        

        const createdArtist = await newArtist.save();
        
        return res.status(201).jsonrender('artist', { artist: createdArtist });

    } catch (error) {
        return next(error);
    }
};





const findArtistById = async (req, res, next) => {

    const { id } = req.params;

    try {
        const artist = await Artist.findById(id);
        return res.render('artist', { artist });
    } catch(error) {
        return next(error);
    }
};


const addToStudy = async (req, res, next) => {
    try {
      const studyId = req.body.studyId;
      const artistId = req.body.artistId;
  
      const updatedStudy = await Study.findByIdAndUpdate(
        studyId,
        { $push: { artist: artistId } },
        { new: true }
      );
  
      return res.status(200).json(updatedStudy);
    } catch (err) {
      next(err);
    }
  };



const addStudy = async (req, res, next) => {
    try {
      const studyId = req.body.studyId;
      const artistId = req.body.artistId;
  
      const updatedShelter = await Shelter.findByIdAndUpdate(
        shelterId,
        { $push: { pets: petId } },
        { new: true }
      );
  
      return res.status(200).json(updatedShelter);
    } catch (err) {
      next(err);
    }
  };




  module.exports = {
    artistEditPut,
    findArtist,
    createArtistGet,
    newArtist,
    findArtistById,
    addToStudy,
    addStudy,
}