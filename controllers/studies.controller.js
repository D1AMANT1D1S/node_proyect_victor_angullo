const Study = require('../models/study.models');

const allStudyGet = async (req, res, next) => {
    try {
        const studies = await Study.find().populate('artists');
        return res.render('studies/studies', { studies });
    } catch (error) {
        next(error);
    }
};

const createStudyPost = async (req, res, next) => {
    try {
        const { name, location, style, artists } = req.body;

        const newStudy = new Study({ name, location, style, artists });

        const createdStudy = await newStudy.save();

        return res.status(201).json(createdStudy);
    } catch (error) {
        next(error);
    }
};

const addArtistPut = async (req, res, next) => {
    try {
        const { studyId, artistId } = req.body;

        const updatedStudy = await Study.findByIdAndUpdate(
            studyId,
            { $addToSet: { artists: artistId } },

            { new: true }
        );

        return res.status(200).json(updatedStudy);
        
    } catch(error) {
        next(error);
    }
};

const studyByIdGet = async (req, res, next) => {

    const { id } = req.params;

    try {
        const study = await Study.findById(id).populate('artists');
        return res.render('stuides/studies', { study, name: 'Bangarang Tattoo' });
    } catch(error) {
        return next(error);
    }
};

const deleteStudyByIdPost = async (req, res, next) => {
    try {
        const { id } = req.params;
        await Study.findByIdAndDelete(id);

        return res.redirect('/studies');
    } catch(error) {
        next(error);
    }
};

module.exports = {
    allStudyGet,
    createStudyPost,
    addArtistPut,
    studyByIdGet,
    deleteStudyByIdPost,
};