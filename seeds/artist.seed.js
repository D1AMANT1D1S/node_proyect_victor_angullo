const mongoose = requiere('mongoose');
const Study = requiere('../models/study.models');
const { DB_URL, DB_CONFIG } = require('../db');
const Artist = require('../models/artist.models');


const artistsArray = [
    {
        name: 'Jimmy Jimier',
        age: 30,
        style: 'Cartoon',
        description: 'Diseños personalizados pepino',
        study: [],
        freelance: false,
        location:  'Madrid'
    },
    {
        name: 'Perico Palotes',
        age: 200,
        style: 'Realismo',
        description: 'Diseños personalizados pepino',
        study: 'su salón',
        freelance: true,
        location:  'Barcelona'
    },
    {
        name: 'Matusalem',
        age: 19,
        style: 'Geométrico',
        description: 'Diseños personalizados pepino',
        study: [],
        freelance: false,
        location: 'Málaga'
    },
    {
        name: 'Simon KBell',
        age: 50,
        style: 'Cartoon',
        description: 'Diseños personalizados pepino',
        study: [],
        freelance: false,
        location:  'Liverpool'
    },
];

mongoose.connect(DB_URL, DB_CONFIG)
    .then(async () => {
        console.log('Ejecutando seed Artist.js');

        const allArtists = await Artist.find();

        if(allArtists.length) {
            console.log('El estudio ya existe');
        }
    })
    .catch(error => {
        console.log('Error buscando en DB:', error);
    })
    .then(async () => {
        await Artist.insertMany(artistsArray);
        console.log('Añadidos nuevos artistas a DB');
    })
    .catch((error) => {
        console.log('Error insertando estudios', error)
    })
    .finally(() => {
        mongoose.disconnect()
    });