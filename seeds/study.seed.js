const mongoose = requiere('mongoose');
const Study = requiere('../models/study.models');
const { DB_URL, DB_CONFIG } = require('../db');


const studiesArray = [
    {
        name: 'Bangarang Tattoo',
        style: 'Cartoon',
        location: 'Madrid',
        artists: []
    },
    {
        name: 'Samurai Shop',
        style: 'Realismo',
        location: 'Barcelona',
        artists: []
    },
    {
        name: 'Seven Tattoo',
        style: 'Geometrico',
        location: 'Malaga',
        artists: []
    },
    {
        name: 'Black Waltz',
        style: 'Cartoon',
        location: 'Liverpool',
        artists: []
    },
];

mongoose.connect(DB_URL, DB_CONFIG)
    .then(async () => {
        console.log('Ejecutando seed Study.js');

        const allStudies = await Study.find();

        if(allStudies.length) {
            console.log('El estudio ya existe');
        }
    })
    .catch(error => {
        console.log('Error buscando en DB:', error);
    })
    .then(async () => {
        await Study.insertMany(studiesArray);
        console.log('Añadidos nuevos estudios a DB');
    })
    .catch((error) => {
        console.log('Error insertando estudios', error)
    })
    .finally(() => {
        mongoose.disconnect()
    });