const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const studySchema = new Schema(
    {
        name: { type: String, required: true },
        style: { type: String, required: true },
        location:  { type: String, required: true  },
        artists: [{ type: mongoose.Types.ObjectId, ref:"Artists"}], 
        admin: {type: mongoose.Types.ObjectId, ref:"Users"}
    },
    { timestamps: true }
);

const Study = mongoose.model('Studies', studySchema);

module.exports = Study;