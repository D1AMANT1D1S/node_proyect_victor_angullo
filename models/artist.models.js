const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const artistSchema = new Schema(
    {
        name: { type: String, required: true },
        age: { type: Number },
        style: { type: String, required: true },
        description: { type: String },
        study: { type: mongoose.Types.ObjectId, ref:"Studies"},
        freelance: { type: Boolean, required: true },
        location:  { type: String, required: true  }
    },
    { timestamps: true }
);

const Artist = mongoose.model('Artists', artistSchema);

module.exports = Artist;